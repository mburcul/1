//
//  CityWeatherViewModel.swift
//  WeatherApp
//
//  Created by Marko Burčul on 02/07/2018.
//  Copyright © 2018 Marko Burčul. All rights reserved.
//

import Foundation
import AERecord
import CoreData
import Alamofire

class CityWeatherViewModel{
    var cities: [City]! = []
    let appID:String = "62465b180341e3c7766e720bf327e0aa"
    let citiesID:String = "3186886,3190261,3193935,3191648"
    var searchString:String = ""
    
    func filterCities(){
        let request: NSFetchRequest<City> = City.fetchRequest()
        let descriptor = NSSortDescriptor(key: "name", ascending: true, selector:#selector(NSString.localizedStandardCompare(_:)))
        if(self.searchString != ""){
            request.predicate = NSPredicate(format: "name CONTAINS[c] %@", self.searchString )
        }else{
            let pred1 = NSPredicate(format: "name = %@", "Zagreb")
            let pred2 = NSPredicate(format: "name = %@", "Split")
            let pred3 = NSPredicate(format: "name = %@", "Osijek")
            let pred4 = NSPredicate(format: "name = %@", "Rijeka")
            request.predicate = NSCompoundPredicate(orPredicateWithSubpredicates: [pred1, pred2, pred3, pred4])
        }
        request.sortDescriptors = [descriptor]
        let context = AERecord.Context.main
        self.cities = try! context.fetch(request)
    }
    
    func fetchWeather(completion:@escaping (() -> Void)) -> Void {
        let url:String = "https://api.openweathermap.org/data/2.5/group?units=metric"
        Alamofire.request(url,
                            method: .get,
                            parameters: ["appid":appID,
                                        "id":citiesID
                                        ])
                .validate()
                .responseJSON{ response in
                    guard response.result.isSuccess else {
                        return
                    }
                    if
                        let results = response.result.value as? [String: Any],
                        let listOfCities = results["list"] as? [[String:Any]]{
                        for case let cityWeatherData in listOfCities{
                            self.handleEntity(json: cityWeatherData)
                        }
                        completion()
                        return
                    } else {
                        return
                    }
            }
    }
    
    func searchWeatherIn(cityName: String, completion:@escaping (() -> Void)) -> Void {
        let url:String = "https://api.openweathermap.org/data/2.5/weather?units=metric"
        Alamofire.request(url,
                          method: .get,
                          parameters: ["appid":appID,
                                       "q":cityName
            ])
            .validate()
            .responseJSON{ response in
                guard response.result.isSuccess else {
                    return
                }
                if
                    let cityWeatherData = response.result.value as? [String: Any]{
                    self.handleEntity(json: cityWeatherData)
                    completion()
                    return
                } else {
                    return
                }
        }
    }
    
    func handleEntity(json: [String:Any]) -> Void{
        if
            let name = json["name"] as? String,
            let weather = json["weather"] as? [[String:Any]],
            let weatherState = weather[0]["main"] as? String,
            let weathericon = weather[0]["icon"] as? String,
            let main = json["main"] as? [String:Any],
            let pressure = main["pressure"] as? Int,
            let humidity = main["humidity"] as? Int,
            let tempMin = main["temp_min"] as? Int,
            let tempMax = main["temp_max"] as? Int{

            let city:City = City.firstOrCreate(with: ["name": name])
            city.humidity = Float(humidity)
            city.pressure = Int32(pressure)
            city.temperatureMax = Int32(tempMax)
            city.temperatureMin = Int32(tempMin)
            city.weatherState = weatherState
            city.iconcode = weathericon
            
            try? AERecord.Context.main.save()
        }
    }
    
    func numberOfCities()->Int{
        return cities.count
    }
    
    func city(atIndex index: Int) -> City? {
        guard let cities = cities else {
            return nil
        }
        return cities[index]
    }
    
}

