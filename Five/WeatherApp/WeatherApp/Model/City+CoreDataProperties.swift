//
//  City+CoreDataProperties.swift
//  WeatherApp
//
//  Created by Marko Burčul on 03/07/2018.
//  Copyright © 2018 Marko Burčul. All rights reserved.
//
//

import Foundation
import CoreData


extension City {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<City> {
        return NSFetchRequest<City>(entityName: "City")
    }

    @NSManaged public var humidity: Float
    @NSManaged public var name: String?
    @NSManaged public var pressure: Int32
    @NSManaged public var temperatureMax: Int32
    @NSManaged public var temperatureMin: Int32
    @NSManaged public var weatherState: String?
    @NSManaged public var iconcode: String?
}
