//
//  WeatherDetailsViewController.swift
//  WeatherApp
//
//  Created by Marko Burčul on 02/07/2018.
//  Copyright © 2018 Marko Burčul. All rights reserved.
//

import UIKit

class WeatherDetailsViewController: UIViewController {

    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var weatherLogo: UIImageView!
    @IBOutlet weak var weatherDescriptionLabel: UILabel!
    @IBOutlet weak var tempMinLabel: UILabel!
    @IBOutlet weak var tempMaxLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var pressureLabel: UILabel!
    
    var cityData:City!
    
    convenience init(city: City){
        self.init()
        self.cityData = city
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupData()
    }
    
    func setupData(){
        self.cityNameLabel.text = cityData.name
        self.weatherLogo.image = UIImage(named: cityData.iconcode!)
        self.weatherDescriptionLabel.text = cityData.weatherState
        self.tempMinLabel.text = "Temperature min: " + String(cityData.temperatureMin) + " C"
        self.tempMaxLabel.text = "Temperature max: " + String(cityData.temperatureMax) + " C"
        self.humidityLabel.text = "Humidity level: " + String(cityData.humidity) + " %"
        self.pressureLabel.text = "Pressure: " + String(cityData.pressure) + " Pa"
    }

}
