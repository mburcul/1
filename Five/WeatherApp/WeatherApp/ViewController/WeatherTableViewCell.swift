//
//  WeatherTableViewCell.swift
//  WeatherApp
//
//  Created by Marko Burčul on 02/07/2018.
//  Copyright © 2018 Marko Burčul. All rights reserved.
//

import UIKit


class WeatherTableViewCell: UITableViewCell {

    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var weatherIcon: UIImageView!
    @IBOutlet weak var minTempLabel: UILabel!
    @IBOutlet weak var maxTempLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        cityNameLabel.text = ""
        weatherIcon.image = nil
        minTempLabel.text = ""
        maxTempLabel.text = ""
    }
    
    func setup(withCity city: City) {
        cityNameLabel.text = city.name
        minTempLabel.text = "Temperature min: " + String(city.temperatureMin)
        maxTempLabel.text = "Temperature max: " + String(city.temperatureMax)
        weatherIcon.image = UIImage(named: city.iconcode!)
    }
    
}
