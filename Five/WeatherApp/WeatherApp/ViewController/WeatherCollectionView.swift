//
//  WeatherCollectionView.swift
//  WeatherApp
//
//  Created by Marko Burčul on 02/07/2018.
//  Copyright © 2018 Marko Burčul. All rights reserved.
//

import Foundation
import UIKit

class WeatherTableViewController: UITableViewController{

    let cellReuseIdentifier = "weatherCell"
    let viewModel:CityWeatherViewModel = CityWeatherViewModel()
    let searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupData()
        setupTableView()
        searchController.searchBar.delegate = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search weather in..."
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }
    
    func setupData(){
        viewModel.fetchWeather { [weak self] in
            self?.reloadData()
        }
    }
    
    func setupTableView(){
        self.tableView.backgroundColor = UIColor.white
        self.tableView.delegate = self
        self.tableView.dataSource = self
        tableView.separatorColor = UIColor.white
        tableView.register(UINib(nibName: "WeatherTableViewCell", bundle: nil), forCellReuseIdentifier: cellReuseIdentifier)
        
    }
  
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfCities()
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! WeatherTableViewCell
        
        if let city = viewModel.city(atIndex: indexPath.row) {
            cell.setup(withCity: city)
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 200.0
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let city = viewModel.city(atIndex: indexPath.row){
            let wdc = WeatherDetailsViewController(city: city)
            navigationController?.pushViewController(wdc, animated: true)
        }

    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 150
    }
    
    func reloadData(){
        self.viewModel.filterCities()
        self.tableView.reloadData()
    }
    
    func filterContentForSearchText(_ cityName: String) {
        if(cityName == ""){
            viewModel.searchString = ""
            viewModel.fetchWeather { [weak self] in
                self?.reloadData()
            }
        }else{
            viewModel.searchString = cityName
            viewModel.searchWeatherIn(cityName: cityName, completion: { [weak self] in
                self?.reloadData()
            })
        }
    }

}

extension WeatherTableViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        filterContentForSearchText(searchController.searchBar.text!)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        filterContentForSearchText(searchController.searchBar.text!)
    }

}
